/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package wbrsolutions.mbcheckfx;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.util.List;
import wbrsolutions.mbcheckfx.model.Orderbook;
import wbrsolutions.mbcheckfx.model.Ticker;
import wbrsolutions.mbcheckfx.model.Trade;
import wbrsolutions.mbcheckfx.model.util.HtmlUtil;

/**
 *
 * @author Wellington Wagner (wellingtonwa@gmail.com)
 */
public class APIMercadoBitcoin {

    private static final String URL_ORDERBOOK_LITECOIN = "https://www.mercadobitcoin.com.br/api/orderbook_litecoin/";
    private static final String URL_ORDERBOOK_BITCOIN = "https://www.mercadobitcoin.com.br/api/orderbook/";
    private static final String URL_TICKER_LITECOIN = "https://www.mercadobitcoin.com.br/api/ticker_litecoin/";
    private static final String URL_TICKER_BITCOIN = "https://www.mercadobitcoin.com.br/api/ticker/";
    private static final String URL_TRADES_BITCOIN = "https://www.mercadobitcoin.com.br/api/trades/";
    private static final String URL_TRADES_LITECOIN = "https://www.mercadobitcoin.com.br/api/trades_litecoin/";
    
    public static Orderbook getOrderbookLitecoin() throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        String json = HtmlUtil.getHtml(URL_ORDERBOOK_LITECOIN);
        return mapper.readValue(json, Orderbook.class);
    }
    
    public static Ticker getTickerLitecoin() throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.UNWRAP_ROOT_VALUE, true);
        String json = HtmlUtil.getHtml(URL_TICKER_LITECOIN);
        return mapper.readValue(json, Ticker.class);
    }
    
    public static Orderbook getOrderbookBitcoin() throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        String json = HtmlUtil.getHtml(URL_ORDERBOOK_BITCOIN);
        return mapper.readValue(json, Orderbook.class);
    }
    
    public static Ticker getTickerBitcoin() throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.UNWRAP_ROOT_VALUE, true);
        String json = HtmlUtil.getHtml(URL_TICKER_BITCOIN);
        return mapper.readValue(json, Ticker.class);
    }
    
    public static List<Trade> getTradesBitcoin() throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        String json = HtmlUtil.getHtml(URL_TRADES_BITCOIN);
        return mapper.readValue(json, new TypeReference<List<Trade>>(){});
    }
    
    public static List<Trade> getTradesLitecoin() throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        String json = HtmlUtil.getHtml(URL_TRADES_LITECOIN);
        return mapper.readValue(json, new TypeReference<List<Trade>>(){});
    }
    
    
}
