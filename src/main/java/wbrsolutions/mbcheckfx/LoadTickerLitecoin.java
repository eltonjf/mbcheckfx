/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wbrsolutions.mbcheckfx;

import java.io.IOException;
import java.math.RoundingMode;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.scene.control.Label;
import wbrsolutions.mbcheckfx.model.Ticker;

/**
 *
 * @author Wellington Wagner (wellingtonwa@gmail.com)
 */
public class LoadTickerLitecoin extends TimerTask {

    private final Label lastPrice;
    private final Label highPrice;
    private final Label lowPrice;
    private final Label vol;

    public LoadTickerLitecoin(Label lastPrice, Label highPrice, Label lowPrice, Label vol) {
        this.lastPrice = lastPrice;
        this.highPrice = highPrice;
        this.lowPrice = lowPrice;
        this.vol = vol;
    }

    @Override
    public void run() {
        Platform.runLater(() -> {
            lastPrice.setText("-");
            highPrice.setText("-");
            lowPrice.setText("-");
            vol.setText("-");
        });
        Ticker ticker = null;
        try {
            ticker = APIMercadoBitcoin.getTickerLitecoin();
        } catch (IOException ex) {
            Logger.getLogger(LoadTickerLitecoin.class.getName()).log(Level.SEVERE, null, ex);
        }

        if (ticker != null && ticker.getDate() != null) {
            String lp = ticker.getLast().setScale(2, RoundingMode.HALF_EVEN).toPlainString();
            String hp = ticker.getHigh().setScale(2, RoundingMode.HALF_EVEN).toPlainString();
            String lop = ticker.getLow().setScale(2, RoundingMode.HALF_EVEN).toPlainString();
            String v = ticker.getVol().setScale(3, RoundingMode.HALF_EVEN).toPlainString();
            Platform.runLater(() -> {
                lastPrice.setText(lp);
                highPrice.setText(hp);
                lowPrice.setText(lop);
                vol.setText(v);
            });
        }
    }

}
