/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wbrsolutions.mbcheckfx;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.scene.control.Label;
import javafx.scene.control.TableView;
import wbrsolutions.mbcheckfx.model.Compra;
import wbrsolutions.mbcheckfx.model.Venda;

/**
 *
 * @author Wellington Wagner (wellingtonwa@gmail.com)
 */
public class LoadOrderbookBitcoin extends TimerTask {

    private final TableView tableAsks;
    private final TableView tableBids;
    private final Label label;

    public LoadOrderbookBitcoin(TableView tableAsks, TableView tableBids, Label label) {
        this.tableAsks = tableAsks;
        this.tableBids = tableBids;
        this.label = label;
    }

    @Override
    public void run() {
        List<Compra> compras = null;
        List<Venda> vendas = null;
        try {
            vendas = APIMercadoBitcoin.getOrderbookBitcoin().getAsks();
            compras = APIMercadoBitcoin.getOrderbookBitcoin().getBids();
        } catch (IOException ex) {
            Logger.getLogger(LoadOrderbookBitcoin.class.getName()).log(Level.SEVERE, null, ex);
        }
        ObservableList<Venda> dadosVenda = FXCollections.observableArrayList(vendas);
        ObservableList<Compra> dadosCompra = FXCollections.observableArrayList(compras);
        Platform.runLater(() -> {
            label.setText(new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(new Date()));
            tableAsks.setItems(dadosVenda);
            tableBids.setItems(dadosCompra);
        });
    }

}
