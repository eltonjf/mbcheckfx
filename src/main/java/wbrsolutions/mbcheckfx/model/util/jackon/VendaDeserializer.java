/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package wbrsolutions.mbcheckfx.model.util.jackon;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import wbrsolutions.mbcheckfx.model.ICompraVenda;
import wbrsolutions.mbcheckfx.model.Venda;

/**
 *
 * @author Wellington Wagner (wellingtonwa@gmail.com)
 */
public class VendaDeserializer extends JsonDeserializer<List<Venda>>{

    @Override
    public List<Venda> deserialize(JsonParser jp, DeserializationContext dc) throws IOException, JsonProcessingException {
        JsonNode node = jp.getCodec().readTree(jp);
        List<Venda> result = new ArrayList<>();
        for(int i=0;i<node.size();i++){
            JsonNode auxNode = node.get(i);
            Venda venda = new Venda();
            venda.setPreco(new BigDecimal(auxNode.get(0).asDouble()));
            venda.setVolume(new BigDecimal(auxNode.get(1).asDouble()));
            if(venda.getPreco()!=null){
                venda.setPreco(venda.getPreco().setScale(5, RoundingMode.HALF_EVEN));
                venda.setVolume(venda.getVolume().setScale(5, RoundingMode.HALF_EVEN));
            }
            result.add(venda);
        }
        return result;
    }

}
