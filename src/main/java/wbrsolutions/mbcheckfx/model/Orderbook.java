/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package wbrsolutions.mbcheckfx.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import java.util.List;
import wbrsolutions.mbcheckfx.model.util.jackon.CompraDeserializer;
import wbrsolutions.mbcheckfx.model.util.jackon.VendaDeserializer;

/**
 *
 * @author Wellington Wagner (wellingtonwa@gmail.com)
 */
public class Orderbook {
    
    @JsonDeserialize(using = VendaDeserializer.class)
    private List<Venda> asks;
    @JsonDeserialize(using = CompraDeserializer.class)
    private List<Compra> bids;

    public List<Venda> getAsks() {
        return asks;
    }

    public void setAsks(List<Venda> ask) {
        this.asks = ask;
    }

    public List<Compra> getBids() {
        return bids;
    }

    public void setBids(List<Compra> bid) {
        this.bids = bid;
    }

    
    
    
}
