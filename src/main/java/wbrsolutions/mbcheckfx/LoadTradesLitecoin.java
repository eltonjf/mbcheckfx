/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wbrsolutions.mbcheckfx;

import java.io.IOException;
import java.util.List;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.TableView;
import wbrsolutions.mbcheckfx.model.Ticker;
import wbrsolutions.mbcheckfx.model.Trade;

/**
 *
 * @author Wellington Wagner (wellingtonwa@gmail.com)
 */
public class LoadTradesLitecoin extends TimerTask {

    private final TableView tableView;

    public LoadTradesLitecoin(TableView tableView) {
        this.tableView = tableView;
    }

    @Override
    public void run() {
        Ticker ticker = null;
        List<Trade> trades = null;
        try {
            trades = APIMercadoBitcoin.getTradesLitecoin();
        } catch (IOException ex) {
            Logger.getLogger(LoadTradesLitecoin.class.getName()).log(Level.SEVERE, null, ex);
        }

        if (trades!=null && !trades.isEmpty()) {
            ObservableList<Trade> dados = FXCollections.observableArrayList(trades);
            Platform.runLater(() -> {
                tableView.setItems(dados);
            });
        }
    }

}
