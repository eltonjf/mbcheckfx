package wbrsolutions.mbcheckfx;

import java.math.BigDecimal;
import java.net.URL;
import java.util.Date;
import java.util.ResourceBundle;
import java.util.Timer;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import wbrsolutions.mbcheckfx.model.Compra;
import wbrsolutions.mbcheckfx.model.Trade;
import wbrsolutions.mbcheckfx.model.Venda;

public class FXMLController implements Initializable {

    @FXML
    private Label lblHoraAtualizacao;
    @FXML
    private TableView tbvOrdemCompraLitecoin;
    @FXML
    private TableView tbvOrdemVendaLitecoin;
    @FXML
    private TableView tbvOrdemCompraBitcoin;
    @FXML
    private TableView tbvOrdemVendaBitcoin;
    @FXML
    private TableView tbvTradesBitcoin;
    @FXML
    private TableView tbvTradesLitecoin;
    @FXML
    private TableColumn<Compra, BigDecimal> compraVolumeLitecoin;
    @FXML
    private TableColumn<Compra, BigDecimal> compraValorLitecoin;
    @FXML
    private TableColumn<Venda, BigDecimal> vendaVolumeLitecoin;
    @FXML
    private TableColumn<Venda, BigDecimal> vendaValorLitecoin;
    @FXML
    private TableColumn<Compra, BigDecimal> compraVolumeBitcoin;
    @FXML
    private TableColumn<Compra, BigDecimal> compraValorBitcoin;
    @FXML
    private TableColumn<Venda, BigDecimal> vendaVolumeBitcoin;
    @FXML
    private TableColumn<Venda, BigDecimal> vendaValorBitcoin;
    // Columns of trade bitcoin tableview
    @FXML
    private TableColumn<Trade, Date> dateTradeBitcoin;
    @FXML
    private TableColumn<Trade, String> typeTradeBitcoin;
    @FXML
    private TableColumn<Trade, BigDecimal> amountTradeBitcoin;
    @FXML
    private TableColumn<Trade, BigDecimal> priceTradeBitcoin;
    // Columns of trade litecoin tableview
    @FXML
    private TableColumn<Trade, Date> dateTradeLitecoin;
    @FXML
    private TableColumn<Trade, String> typeTradeLitecoin;
    @FXML
    private TableColumn<Trade, BigDecimal> amountTradeLitecoin;
    @FXML
    private TableColumn<Trade, BigDecimal> priceTradeLitecoin;
    
    @FXML
    private Label highPriceLitecoin;
    @FXML
    private Label lowPriceLitecoin;
    @FXML
    private Label lastPriceLitecoin;
    @FXML
    private Label volLitecoin;
    @FXML
    private Label highPriceBitcoin;
    @FXML
    private Label lowPriceBitcoin;
    @FXML
    private Label lastPriceBitcoin;
    @FXML
    private Label volBitcoin;
    
    private Timer timer = new Timer(true);

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        compraValorLitecoin.setCellValueFactory(new PropertyValueFactory<>("preco"));
        compraVolumeLitecoin.setCellValueFactory(new PropertyValueFactory<>("volume"));
        vendaValorLitecoin.setCellValueFactory(new PropertyValueFactory<>("preco"));
        vendaVolumeLitecoin.setCellValueFactory(new PropertyValueFactory<>("volume"));
        compraValorBitcoin.setCellValueFactory(new PropertyValueFactory<>("preco"));
        compraVolumeBitcoin.setCellValueFactory(new PropertyValueFactory<>("volume"));
        vendaValorBitcoin.setCellValueFactory(new PropertyValueFactory<>("preco"));
        vendaVolumeBitcoin.setCellValueFactory(new PropertyValueFactory<>("volume"));
        //Bitcoin trades
        dateTradeBitcoin.setCellValueFactory(new PropertyValueFactory<>("data"));
        typeTradeBitcoin.setCellValueFactory(new PropertyValueFactory<>("tipo"));
        amountTradeBitcoin.setCellValueFactory(new PropertyValueFactory<>("amount"));
        priceTradeBitcoin.setCellValueFactory(new PropertyValueFactory<>("price"));
        //Litecoin trades
        dateTradeLitecoin.setCellValueFactory(new PropertyValueFactory<>("data"));
        typeTradeLitecoin.setCellValueFactory(new PropertyValueFactory<>("tipo"));
        amountTradeLitecoin.setCellValueFactory(new PropertyValueFactory<>("amount"));
        priceTradeLitecoin.setCellValueFactory(new PropertyValueFactory<>("price"));

        this.preencherTabelasNegociacoes();
        this.preencherTicker();
        this.preencherTrades();
    }

    public void preencherTabelasNegociacoes() {
        timer.scheduleAtFixedRate(new LoadOrderbookLitecoin(tbvOrdemVendaLitecoin, tbvOrdemCompraLitecoin, lblHoraAtualizacao), 0, 1000 * 10);
        timer.scheduleAtFixedRate(new LoadOrderbookBitcoin(tbvOrdemVendaBitcoin, tbvOrdemCompraBitcoin, lblHoraAtualizacao), 0, 1000 * 10);
    }
    
    public void preencherTicker() {
        timer.scheduleAtFixedRate(new LoadTickerLitecoin(lastPriceLitecoin, highPriceLitecoin
                , lowPriceLitecoin, volLitecoin), 0, 1000 * 10);
        timer.scheduleAtFixedRate(new LoadTickerBitcoin(lastPriceBitcoin, highPriceBitcoin
                , lowPriceBitcoin, volBitcoin), 0, 1000 * 10);
    }
    
    public void preencherTrades() {
        timer.scheduleAtFixedRate(new LoadTradesBitcoin(tbvTradesBitcoin), 0, 1000*10);
        timer.scheduleAtFixedRate(new LoadTradesLitecoin(tbvTradesLitecoin), 0, 1000*10);
    }
    
    
    
}
